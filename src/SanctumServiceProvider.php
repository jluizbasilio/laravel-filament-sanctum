<?php

namespace JluizBasilio\Sanctum;

use JluizBasilio\Sanctum\Pages\Sanctum;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;

class SanctumServiceProvider extends PluginServiceProvider
{
    protected array $pages = [
        Sanctum::class,
    ];

    protected array $styles = [
        'laravel-filament-sanctum' => __DIR__.'/../resources/dist/app.css',
    ];

    public function configurePackage(Package $package): void
    {
        $package
            ->name('laravel-filament-sanctum')
            ->hasViews()
            ->hasConfigFile()
            ->hasAssets('laravel-filament-sanctum')
            ->hasTranslations();
    }
}
